# -*- coding: utf-8 -*-
# codiname: shitzen

print("""
 _____             _       _                   _       ______ _                  _       
|_   _|           | |     | |                 | |      | ___ (_)                (_)      
  | |_ __ __ _  __| |_   _| |_ ___  _ __    __| | ___  | |_/ /_ _ __   __ _ _ __ _  ___  
  | | '__/ _` |/ _` | | | | __/ _ \| '__|  / _` |/ _ \ | ___ \ | '_ \ / _` | '__| |/ _ \ 
  | | | | (_| | (_| | |_| | || (_) | |    | (_| |  __/ | |_/ / | | | | (_| | |  | | (_) |
  \_/_|  \__,_|\__,_|\__,_|\__\___/|_|     \__,_|\___| \____/|_|_| |_|\__,_|_|  |_|\___/ 
-----------------------------------------------------------------------------------------
   \n""")

def codificar(text):
	cod = text
	cod = cod.replace("A", "01000001"+" ")
	cod = cod.replace("a", "01100001"+" ")
	cod = cod.replace("B", "01000010"+" ")
	cod = cod.replace("b", "01100010"+" ")
	cod = cod.replace("C", "01000011"+" ")
	cod = cod.replace("c", "01100011"+" ")
	cod = cod.replace("D", "01000100"+" ")
	cod = cod.replace("d", "01100100"+" ")
	cod = cod.replace("E", "01000101"+" ")
	cod = cod.replace("e", "01100101"+" ")
	cod = cod.replace("F", "01000110"+" ")
	cod = cod.replace("f", "01100110"+" ")
	cod = cod.replace("G", "01000111"+" ")
	cod = cod.replace("g", "01100111"+" ")
	cod = cod.replace("H", "01001000"+" ")
	cod = cod.replace("h", "01101000"+" ")
	cod = cod.replace("I", "01001001"+" ")
	cod = cod.replace("i", "01101001"+" ")
	cod = cod.replace("J", "01001010"+" ")
	cod = cod.replace("j", "01101010"+" ")
	cod = cod.replace("K", "01001011"+" ")
	cod = cod.replace("k", "01101011"+" ")
	cod = cod.replace("L", "01001100"+" ")
	cod = cod.replace("l", "01101100"+" ")
	cod = cod.replace("M", "01001101"+" ")
	cod = cod.replace("m", "01101101"+" ")
	cod = cod.replace("N", "01001110"+" ")
	cod = cod.replace("n", "01101110"+" ")
	cod = cod.replace("O", "01001111"+" ")
	cod = cod.replace("o", "01101111"+" ")
	cod = cod.replace("P", "01010000"+" ")
	cod = cod.replace("p", "01110000"+" ")
	cod = cod.replace("Q", "01010001"+" ")
	cod = cod.replace("q", "01110001"+" ")
	cod = cod.replace("R", "01010010"+" ")
	cod = cod.replace("r", "01110010"+" ")
	cod = cod.replace("S", "01010011"+" ")
	cod = cod.replace("s", "01110011"+" ")
	cod = cod.replace("T", "01010100"+" ")
	cod = cod.replace("t", "01110100"+" ")
	cod = cod.replace("U", "01010101"+" ")
	cod = cod.replace("u", "01110101"+" ")
	cod = cod.replace("V", "01010110"+" ")
	cod = cod.replace("v", "01110110"+" ")
	cod = cod.replace("W", "01010111"+" ")
	cod = cod.replace("w", "01110111"+" ")
	cod = cod.replace("X", "01011000"+" ")
	cod = cod.replace("x", "01111000"+" ")
	cod = cod.replace("Y", "01011001"+" ")
	cod = cod.replace("y", "01111001"+" ")
	cod = cod.replace("Z", "01011010"+" ")
	cod = cod.replace("z", "01111010"+" ")
	cod = cod.replace("?", "00111111"+" ")
	cod = cod.replace("!", "00100001"+" ")
	cod = cod.replace(".", "00101110"+" ")
	cod = cod.replace(",", "00101100"+" ")
	cod = cod.replace(";", "00111011"+" ")
	cod = cod.replace(">", "00111110"+" ")
	cod = cod.replace("<", "00111100"+" ")
	cod = cod.replace(":", "00111010"+" ")
	cod = cod.replace("/", "00101111"+" ")
	cod = cod.replace('\\', "01011100"+" ")
	print("\n[+] Mensagem codificada: {0}" .format(cod))

def decoficar(text):
	cod = text
	cod = cod.replace("01000001 ", "A")
	cod = cod.replace("01100001 ", "a")
	cod = cod.replace("01000010 ", "B")
	cod = cod.replace("01100010 ", "b")
	cod = cod.replace("01000011 ", "C")
	cod = cod.replace("01100011 ", "c")
	cod = cod.replace("01000100 ", "D")
	cod = cod.replace("01100100 ", "d")
	cod = cod.replace("01000101 ", "E")
	cod = cod.replace("01100101 ", "e")
	cod = cod.replace("01000110 ", "F")
	cod = cod.replace("01100110 ", "f")
	cod = cod.replace("01000111 ", "G")
	cod = cod.replace("01100111 ", "g")
	cod = cod.replace("01001000 ", "H")
	cod = cod.replace("01101000 ", "h")
	cod = cod.replace("01001001 ", "I")
	cod = cod.replace("01101001 ", "i")
	cod = cod.replace("01001010 ", "J")
	cod = cod.replace("01101010 ", "j")
	cod = cod.replace("01001011 ", "K")
	cod = cod.replace("01101011 ", "k")
	cod = cod.replace("01001100 ", "L")
	cod = cod.replace("01101100 ", "l")
	cod = cod.replace("01001101 ", "M")
	cod = cod.replace("01101101 ", "m")
	cod = cod.replace("01001110 ", "N")
	cod = cod.replace("01101110 ", "n")
	cod = cod.replace("01001111 ", "O")
	cod = cod.replace("01101111 ", "o")
	cod = cod.replace("01010000 ", "P")
	cod = cod.replace("01110000 ", "p")
	cod = cod.replace("01010001 ", "Q")
	cod = cod.replace("01110001 ", "q")
	cod = cod.replace("01010010 ", "R")
	cod = cod.replace("01110010 ", "r")
	cod = cod.replace("01010011 ", "S")
	cod = cod.replace("01110011 ", "s")
	cod = cod.replace("01010100 ", "T")
	cod = cod.replace("01110100 ", "t")
	cod = cod.replace("01010101 ", "U")
	cod = cod.replace("01110101 ", "u")
	cod = cod.replace("01010110 ", "V")
	cod = cod.replace("01110110 ", "v")
	cod = cod.replace("01010111 ", "W")
	cod = cod.replace("01011000 ", "w")
	cod = cod.replace("01110111 ", "X")
	cod = cod.replace("01111000 ", "x")
	cod = cod.replace("01011001 ", "Y")
	cod = cod.replace("01111001 ", "y")
	cod = cod.replace("01011010 ", "Z")
	cod = cod.replace("01111010 ", "z")
	cod = cod.replace("00111111 ", "?")
	cod = cod.replace("00100001 ", "!")
	cod = cod.replace("00101110 ", ".")
	cod = cod.replace("00101100 ", ",")
	cod = cod.replace("00111011 ", ";")
	cod = cod.replace("00111010 ", ":")
	cod = cod.replace("00111110 ", ">")
	cod = cod.replace("00111100 ", "<")
	cod = cod.replace("00101111 ", "/")
	cod = cod.replace("01011100 ", "\\")
	print("\n[+] Mensagem codificada: {0}" .format(cod))


co_de = raw_input("[1] Codificar   [+] \n[2] Decodificar [+] \n\n>> ")

if co_de == "1":
	text = raw_input("\n[+] Digite a mensagem: ")
	codificar(text)

if co_de == "2":
	text = raw_input("\n[+] Digite o binario: ")
	text = text + " "
	decoficar(text)
